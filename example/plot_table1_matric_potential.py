"""
Matrix water potential
======================

Plot evolution water tension in the soil with increasing soil moisture
for different textures.
"""
import matplotlib.pyplot as plt
import numpy as np
from saxton2006.table1 import bubbling_pressure, matric_potential, theta_1500, theta_33, theta_sat

om = 2.5 / 100

data = []
for tex, clay, sand in [
    ('sand', 0.05, 0.88),
    ('silt', 0.05, 0.10),
    ('loam', 0.20, 0.40),
    ('clay', 0.50, 0.25),
]:
    th_sat = theta_sat(clay, sand, om)
    th_fc = theta_33(clay, sand, om)
    th_pwp = theta_1500(clay, sand, om)
    psi_e = bubbling_pressure(clay, sand, om)

    thetas = np.linspace(th_pwp, th_sat, 100)
    psis = [matric_potential(th, th_sat, th_fc, th_pwp, psi_e) for th in thetas]

    data.append([tex, thetas, psis])

fig, axes = plt.subplots(1, 1, figsize=(11, 6), squeeze=False)
ax = axes[0, 0]

for tex, thetas, psis in data:
    ax.plot(thetas, psis, label=tex)

ax.axhline(y=0, ls='--', color='#aaaaaa')
ax.axhline(y=-1.5, ls='--', color='#aaaaaa')

ax.legend(loc='center right')
ax.set_xlabel("Soil moisture [m3.m-3]")
ax.set_ylabel("Matric potential [MPa]")

fig.tight_layout()
plt.show()
